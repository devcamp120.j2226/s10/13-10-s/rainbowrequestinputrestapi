package com.devcamp.rainbowequestinputapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RainbowequestinputapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RainbowequestinputapiApplication.class, args);
	}

}
