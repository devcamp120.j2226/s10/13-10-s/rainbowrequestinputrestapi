package com.devcamp.rainbowequestinputapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.rainbowequestinputapi.service.RainbowService;

@RestController
@CrossOrigin
public class RainbowController {

    @Autowired
    RainbowService rainbowService;
    
    @GetMapping("/rainbow-request-query")
    public ArrayList<String> filterQueryString(@RequestParam(value = "query") String queryString) {
        return rainbowService.filterQuery(queryString);
    }

    @GetMapping("/rainbow-request-param/{index}")
    public String filterIndexParam(@PathVariable(value = "index") int index) {
        return rainbowService.filterIndex(index);
    }
}
