package com.devcamp.rainbowequestinputapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

@Service
public class RainbowService {
    private String[] rainbows = new String[] {"red", "orange", "yellow", "green", "blue", "indigo", "violet"};

    public ArrayList<String> filterQuery(String queryString) {
        ArrayList<String> result = new ArrayList<>();
        for (String color : this.rainbows) {
            if (color.contains(queryString)) {
                result.add(color);
            }
        }

        return result;
    }

    public String filterIndex(int indexParam) {
        String result = "";
        if (indexParam >= 0 && indexParam <= 6) {
          result = this.rainbows[indexParam];
        } 
        return result;
    }    
}
